# Claflin University Chatbot

- Connects Claflin University students to any university faculty
- Stores faculty information as JSON within codebase
- Uses [LUIS](https://www.luis.ai) to implement core AI capabilities
- Implements a multi-turn conversation using Dialogs
- Prompts for and validate requests for information from the user

## Individual Contribution by Anim-Addo Emmanuel

- Worked on scraping Claflin University's faculty directory
- Built the AdaptiveCard interface for the bot
- Here is a link to the my portion of the code: https://bitbucket.org/mcndubuisi/anim-contribution/


## Overview

This bot uses [LUIS](https://www.luis.ai), an AI based cognitive service, to implement language understanding.

- [Node.js](https://nodejs.org) version 10.14 or higher

    ```bash
    # determine node version
    node --version
    ```

## Run this bot locally

- Clone the repository

    ```bash
    git clone https://bitbucket.org/mcndubuisi/claflin_chatbot.git
    ```

- In a terminal, navigate to `claflin_chatbot`

    ```bash
    cd claflin_chatbot
    ```

- Install modules

    ```bash
    npm install
    ```

- Run the sample

    ```bash
    npm start
    ```

## Testing the bot using Bot Framework Emulator

[Bot Framework Emulator](https://github.com/microsoft/botframework-emulator) is a desktop application that allows bot developers to test and debug their bots on localhost or running remotely through a tunnel.

- Install the latest Bot Framework Emulator from [here](https://github.com/Microsoft/BotFramework-Emulator/releases)

## Connect to the bot using Bot Framework Emulator

- Launch Bot Framework Emulator
- File -> Open Bot
- Enter a Bot URL of `http://localhost:3978/api/messages`

